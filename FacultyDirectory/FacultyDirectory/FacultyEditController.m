//
//  DetailViewController.m
//  FacultyDirectory
//
//  Created by Michael Lee on 3/27/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "FacultyEditController.h"
#import "CourseEditController.h"
#import "AppDelegate.h"

@implementation FacultyEditController {
    NSArray *_courses;
}

#pragma mark - Managing the detail item

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.nameField.text = [self.faculty valueForKey:@"name"];
    self.officeField.text = [self.faculty valueForKey:@"office"];
    NSSet *courseSet = [self.faculty valueForKey:@"teaches"];
    _courses = [courseSet sortedArrayUsingDescriptors:@[ [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES] ]];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.faculty setValue:self.nameField.text forKey:@"name"];
    [self.faculty setValue:self.officeField.text forKey:@"office"];
    [(AppDelegate *)[UIApplication sharedApplication].delegate saveContext];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSManagedObject *newCourse = [NSEntityDescription insertNewObjectForEntityForName:@"Course"
                                                              inManagedObjectContext:self.faculty.managedObjectContext];
    [newCourse setValue:self.faculty forKey:@"taught_by"];
    [segue.destinationViewController setCourse:newCourse];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _courses.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Courses";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.textLabel.text = [_courses[indexPath.row] valueForKey:@"id"];
    cell.detailTextLabel.text = [_courses[indexPath.row] valueForKey:@"name"];
    return cell;
}
@end
