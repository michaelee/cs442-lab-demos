//
//  ClassEditController.m
//  FacultyDirectory
//
//  Created by Michael Lee on 3/27/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "CourseEditController.h"
#import "AppDelegate.h"

@implementation CourseEditController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.idField.text = [self.course valueForKey:@"id"];
    self.nameField.text = [self.course valueForKey:@"name"];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.course setValue:self.idField.text forKey:@"id"];
    [self.course setValue:self.nameField.text forKey:@"name"];
    [(AppDelegate *)[UIApplication sharedApplication].delegate saveContext];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

@end
