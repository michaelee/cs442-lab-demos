//
//  ClassEditController.h
//  FacultyDirectory
//
//  Created by Michael Lee on 3/27/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseEditController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) NSManagedObject *course;
@property (weak, nonatomic) IBOutlet UITextField *idField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@end
