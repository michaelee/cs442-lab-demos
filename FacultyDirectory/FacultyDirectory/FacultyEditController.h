//
//  DetailViewController.h
//  FacultyDirectory
//
//  Created by Michael Lee on 3/27/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacultyEditController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSManagedObject *faculty;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *officeField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
