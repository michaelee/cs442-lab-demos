//
//  MasterViewController.h
//  ParseDemo
//
//  Created by Michael Lee on 4/4/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MasterViewController : UITableViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
- (IBAction)doLogout:(id)sender;

@end
