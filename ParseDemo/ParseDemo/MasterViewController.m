//
//  MasterViewController.m
//  ParseDemo
//
//  Created by Michael Lee on 4/4/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"

@interface MasterViewController () {
    NSDateFormatter *_dateFormatter;
    NSMutableArray *_objects;
}
- (void)showLogin;
- (void)loadParseData;
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateStyle = NSDateFormatterShortStyle;
    _dateFormatter.timeStyle = NSDateFormatterMediumStyle;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
}

- (void)viewDidAppear:(BOOL)animated
{
    if (![PFUser currentUser]) {
        [self showLogin];
    } else {
        [self loadParseData];
    }
}

- (void)showLogin
{
    PFLogInViewController *logInController = [[PFLogInViewController alloc] init];
    logInController.delegate = self;
    logInController.signUpController.delegate = self;
    [self presentViewController:logInController animated:YES completion:nil];
}

- (void)loadParseData
{
    PFQuery *query = [PFQuery queryWithClassName:@"AdHoc"];
    [query whereKey:@"owner" equalTo:[PFUser currentUser]];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        _objects = [objects mutableCopy];
        [self.tableView reloadData];
    }];
}

- (IBAction)doLogout:(id)sender {
    [PFUser logOut];
    _objects = [NSMutableArray array];
    [self showLogin];
}

- (void)logInViewController:(PFLogInViewController *)controller
               didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self loadParseData];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    PFObject *newObject = [PFObject objectWithClassName:@"AdHoc"];
    newObject[@"data"] = [_dateFormatter stringFromDate:[NSDate date]];
    newObject.ACL = [PFACL ACLWithUser:[PFUser currentUser]];
    newObject[@"owner"] = [PFUser currentUser];
    [newObject  saveInBackground];
    [_objects insertObject:newObject atIndex:0];

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    PFObject *object = _objects[indexPath.row];
    cell.textLabel.text = object[@"data"];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}
@end
